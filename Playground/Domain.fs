﻿module Domain

open System
open Chiron

type Person =
  { PersonName : Name
    Address : Address
    Job : Job option }
  static member N =
    (fun (person : Person) -> person.PersonName),
    (fun pn (person : Person) -> { person with PersonName = pn })
  static member A =
    (fun (person : Person) -> person.Address),
    (fun a (person : Person) -> { person with Address = a})
  static member J =
    (fun (person : Person) -> person.Job),
    (fun j (person : Person) -> { person with Job = Some j })
  static member ToJson (person : Person) =
    json {
      do! Json.write "personname" person.PersonName
      do! Json.write "address" person.Address
      do! Json.write "job" person.Job
    }
  static member FromJson (_ : Person) =
    json {
      let! personname = Json.read "personname"
      let! address = Json.read "address"
      let! job = Json.read "job"
      return { PersonName = personname; Address = address; Job = job }
    }
and Name =
  { FirstName : string
    MiddleName : string option
    LastName : string option }
  static member FN =
    (fun (name : Name) -> name.FirstName),
    (fun fn (name : Name) -> { name with FirstName = fn })
  static member MN =
    (fun (name : Name) -> name.MiddleName),
    (fun mn (name : Name) -> { name with MiddleName = Some mn })
  static member LN =
    (fun (name : Name) -> name.LastName),
    (fun ln (name : Name) -> { name with LastName = Some ln })
  static member ToJson (name : Name) =
    json {
      do! Json.write "firstname" name.FirstName
      do! Json.write "middlename" name.MiddleName
      do! Json.write "lastname" name.LastName
    }
  static member FromJson (_ : Name) =
    json {
      let! firstname = Json.read "firstname"
      let! middlename = Json.tryRead "middlename"
      let! lastname = Json.tryRead "lastname"
      return { FirstName = firstname; MiddleName = middlename; LastName = lastname }
    }
and Address =
  { District : District
    AltDistrict : District option }
  static member D =
    (fun (address : Address) -> address.District),
    (fun d (address : Address) -> { address with District = d })
  static member AD =
    (fun (address : Address) -> address.AltDistrict),
    (fun ad (address : Address) -> { address with AltDistrict = Some ad })
  static member ToJson (address : Address) =
    json {
      do! Json.write "district" address.District
      do! Json.write "altdistrict" address.AltDistrict
    }
  static member FromJson (_ : Address) =
    json {
      let! district = Json.read "district"
      let! altdistrict = Json.tryRead "altdistrict"
      return { District = district; AltDistrict = altdistrict }
    }
and District =
  { DisName : string
    Street : string option }
  static member DN = 
    (fun (district : District) -> district.DisName),
    (fun dn (district : District) -> { district with DisName = dn })
  static member S = 
    (fun (district : District) -> district.Street),
    (fun s (district : District) -> { district with Street = Some s })
  static member ToJson (district : District) =
    json {
      do! Json.write "disname" district.DisName
      do! Json.write "street" district.Street
    }
  static member FromJson (_ : District) =
    json {
      let! disname = Json.read "disname"
      let! street = Json.tryRead "street"
      return { DisName = disname; Street = street }
    }
and Job =
  { Occupation : string
    Company : Company option }
  static member O = 
    (fun (job : Job) -> job.Occupation),
    (fun o (job : Job) -> { job with Occupation = o })
  static member C =
    (fun (job : Job) -> job.Company),
    (fun c (job : Job) -> { job with Company = Some c })
  static member ToJson (job : Job) =
    json {
      do! Json.write "occupation" job.Occupation
      do! Json.write "company" job.Company
    }
  static member FromJson (_ : Job) =
    json {
      let! occupation = Json.read "occupation"
      let! company = Json.tryRead "company"
      return { Occupation = occupation; Company = company }
    }
  static member Zero =
    { Occupation = String.Empty
      Company = None }
and Company = 
  { CompName : string }
  static member CN = 
    (fun (company : Company) -> company.CompName),
    (fun cn (company : Company) -> { company with CompName = cn })
  static member ToJson (company : Company) =
    json {
      do! Json.write "compname" company.CompName
    }
  static member FromJson (_ : Company) =
    json {
      let! compname = Json.read "compname"
      return { CompName = compname }
    }
and Group =
  { GroupName : string
    Leader : Person
    Members : Person list }
  static member GN =
    (fun (group : Group) -> group.GroupName),
    (fun gn (group : Group) -> { group with GroupName = gn })
  static member L =
    (fun (group : Group) -> group.Leader),
    (fun l (group : Group) -> { group with Leader = l })
  static member M =
    (fun (group : Group) -> group.Members),
    (fun m (group : Group) -> { group with Members = m })
  static member ToJson (group : Group) =
    json {
      do! Json.write "groupname" group.GroupName
      do! Json.write "leader" group.Leader
      do! Json.write "members" group.Members
    }
  static member FromJson (_ : Group) =
    json {
      let! groupname = Json.read "groupname"
      let! leader = Json.read "leader"
      let! members = Json.read "members"
      return { GroupName = groupname; Leader = leader; Members = members }
    }

let districtOne = {
  DisName = "JakUt"
  Street = Some "Petamburan"
}

let districtTwo = {
  DisName = "JakPus"
  Street = None
}

let addressOne = {
  District = districtOne
  AltDistrict = Some districtTwo
}

let nameOne = {
  FirstName = "Melati"
  MiddleName = Some ""
  LastName = Some ""
}

let nameTwo = {
  FirstName = "Angin"
  MiddleName = Some "Mamiri"
  LastName = Some "Kupasang"
}

let companyOne = {
  CompName = "Fireworks Ltd."
}

let jobOne = {
  Occupation = "Sysadmin"
  Company = Some companyOne
}

let personOne = {
  PersonName = nameOne
  Address = addressOne
  Job = Some jobOne
}

let personTwo = {
  PersonName = nameTwo
  Address = addressOne
  Job = None
}

let group = {
  GroupName = "Jaran"
  Leader = personOne
  Members = [ personTwo ]
}


