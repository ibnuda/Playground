﻿module Arkansas

open Orleankka
open Orleankka.FSharp
open System.Linq
open System.Collections.Generic

type Username = string

type Text = string

type Client = ObserverRef

type ClientMessage = 
  | NewMessage of Username * Text
  | Notification of Text

type ServerMessage = 
  | Join of Username * Client
  | Say of Username * Text
  | Disconnect of Username * Client

type ChatServer() = 
  inherit Actor<ServerMessage>()
  let users = Dictionary<Username, IObserverCollection>()
  let notifyClient message = users.Values |> Seq.iter (fun clients -> clients <* message)
  override __.Receive message = 
    task { 
      match message with
      | Join(username, client) -> 
        match users.TryGetValue username with
        | true, clients -> clients.Add client
        | _ -> 
          sprintf "User %A connected." username
          |> Notification
          |> notifyClient
          let clients = ObserverCollection() :> IObserverCollection
          clients.Add client
          users.Add(username, clients)
        return "welcome" |> response
      | Say(username, text) -> 
        (username, text)
        |> NewMessage
        |> notifyClient
        return response()
      | Disconnect(username, client) -> 
        match users.TryGetValue username with
        | true, clients -> 
          if clients.Count() = 1 then users.Remove username |> ignore
          else clients.Remove client
          return response()
        | _ -> return response()
    }

