﻿open System

open Domain
open JasonBourne
open Optik

[<EntryPoint>]
let main argv = 
  printfn "%A" removedValidFromJson
  printfn "----"
  printfn "%A" removedInvalidFromJson
  printfn "----"
//  printfn "%A" deser
  Console.ReadLine() |> ignore
  0
