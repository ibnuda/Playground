﻿module JasonBourne

open Chiron
open Domain

let inline jsonToString something = (Json.serialize >> Json.format) something

let name = jsonToString nameOne
let person = jsonToString personOne

let jsonRemovedValid = """
  {
    "groupname": "Jaran",
    "leader": {
      "address": {
        "altdistrict": {
          "disname": "JakPus",
          "street": null
        },
        "district": {
          "disname": "JakUt",
          "street": "Petamburan"
        }
      },
      "job": {
        "company": {
          "compname": "Fireworks Ltd."
        },
        "occupation": "Sysadmin"
      },
      "personname": {
        "firstname": "Melati",
        "lastname": "",
        "middlename": ""
      }
    },
    "members": [
      {
        "address": {
          "altdistrict": {
            "disname": "JakPus",
            "street": null
          },
          "district": {
            "disname": "JakUt",
            "street": "Petamburan"
          }
        },
        "job": {
          "company": {
            "compname": "Fireworks Ltd."
          },
          "occupation": "Sysadmin"
        },
        "personname": {
          "firstname": "Melati",
          "lastname": "",
          "middlename": ""
        }
      },
      {
        "address": {
          "altdistrict": {
            "disname": "JakPus",
            "street": null
          },
          "district": {
            "disname": "JakUt",
            "street": "Petamburan"
          }
        },
        "job": null,
        "personname": {
          "firstname": "Angin",
          "lastname": "Kupasang",
          "middlename": "Mamiri"
        }
      }
    ]
  }"""

let jsonRemovedInvalid = """
  {
    "groupname": "Jaran",
    },
  }"""

let removedValidFromJson =
  match Json.tryParse jsonRemovedValid with
  | Choice1Of2 json -> Some json
  | Choice2Of2 _ -> None

let removedInvalidFromJson =
  match Json.tryParse jsonRemovedInvalid with
  | Choice1Of2 json -> Some json
  | Choice2Of2 _ -> None

let inline deser something = (Json.parse >> Json.deserialize) something

// TODO: Bind or something. Especially after parsing.
//let binding = Option.bind Json.deserialize removedValidFromJson
let something = Json.bind