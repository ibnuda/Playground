﻿module Optik

open Aether
open Aether.Operators

open Domain

// OPTIC LENS.
// Accessing FirstName of Person
let firstnameOfPerson = Person.N >-> Name.FN
// Accessing data Street of Person
let streetOfPerson = Person.A >-> Address.D >-> District.S
// Also combining a Lens with a Prism. Note the operators
let streetAltOfPerson = Person.A >-> Address.AD >?> District.S

// GET
// Function to get the value of FirstName from a Person.
let fnPerson = Optic.get firstnameOfPerson
// The FirstName value of a personOne, which is an instance of Person.
let firstnameOfPersonOne = Optic.get firstnameOfPerson personOne
// Same as above.
let streetOfPersonOne = Optic.get streetOfPerson personOne

// SET
// Setting the value of FirstName from personOne
let setFirstNamePerson = Optic.set firstnameOfPerson "Mawar" personOne
// Same as above.
let setStreetOfPerson = Optic.set streetOfPerson "ScubaDives. Inc." personOne

// Using a chained function to set FirstName and Street value from a Person instance.
let setFirstNameAndStreetPerson newFN newS victim =
  victim
  |> Optic.set firstnameOfPerson newFN
  |> Optic.set streetOfPerson newS

// Modified data.
let personOneModified = setFirstNameAndStreetPerson "Camellia" "Lenteng Agung" personOne 

// OPTIC PRISM
// Compared to the Lens counterpart, once the operators goes to >?> it can't go back using >->
let nameCompanyPerson = Person.J >?> Job.C >?> Company.CN

// GET
let nameCompanyPersonOne = Optic.get nameCompanyPerson personTwo

// SET
// Creating two new instances of Person from personOne.
let aPersonWithNoJob = Optic.set Person.J Job.Zero personOne
let anotherPersonWithNoJob = Optic.set Person.J Job.Zero personOne
// How to set this prism to None?
// Should we use Zero member or something else?
// I do think that the inability to set the optional type to None is
// not really nice.
// If I set it to Option type, I should be able to set it to None.

// OPTIC LIST

// GET
// A function to get the name of the FirstName value of the first member of a Group.
let getNameHeadMemberOfGroup group =
  match Optic.get List.head_ group.Members with
  | Some person -> Some (fnPerson person)
  | _ -> None
// A function to get the first member of a Group.
let getHeadMember = Optic.get List.head_ group.Members
// Getting data Member from a Group instance.
let getMembersOfGroup = Optic.get Group.M

// SET
// It's shit tbpfh, pham.
let setHeadMemberOfGroup = Optic.set List.head_ personOneModified

// Helpers
// Also shit.
let addMembersOfGroup newMembers = Optic.map id_ (fun lis -> lis @ newMembers)
let newGroupMembers = addMembersOfGroup [ personOne; aPersonWithNoJob; anotherPersonWithNoJob; personTwo ]

// Add member
// This is shit.
let membersOfGroup = getMembersOfGroup group
let addedGroupMembers = newGroupMembers membersOfGroup
let newGroup = Optic.set Group.M addedGroupMembers group

let addMember targetGroup newmembers =
  targetGroup
  |> Optic.get Group.M
  |> List.append newmembers
  |> fun x -> Optic.set Group.M x targetGroup

let newGroupUsingAddMember = addMember group [ personOne; personOneModified ]
let anotherOne = addMember newGroupUsingAddMember [ personOneModified; personOneModified ]

let removeMember targetGroup replaced =
  targetGroup
  |> Optic.get Group.M
  |> List.filter (fun x -> x <> replaced)
  |> fun x -> Optic.set Group.M x targetGroup

let removedModified = removeMember anotherOne personOneModified

let replaceMember targetGroup replaceable replacement =
  targetGroup
  |> Optic.get Group.M
  |> List.filter (fun x -> replaceable.PersonName <> x.PersonName)
  |> List.append replacement
  |> fun x -> Optic.set Group.M x targetGroup

let anotherNewGroup = replaceMember anotherOne personOneModified [ personOne ]
